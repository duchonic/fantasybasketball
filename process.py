import json
import sys

year = 2024
top_players = -1
euro = 'league'

# get 1 to 3 arguments from the cmd line and fill the variables
if len(sys.argv) == 4:
    euro = sys.argv[1]
    year = sys.argv[2]
    top_players = int(sys.argv[3])
elif len(sys.argv) == 3:
    euro = sys.argv[1]
    year = sys.argv[2]
elif len(sys.argv) == 2:
    euro = sys.argv[1]
else:
    year = 2024

print('year: ', year, 'top players: ', top_players)

# Import the requests module
# make a dictionary with all the basketball players
players = {}

# load json data from a file
filename = 'euro' + str(euro) + str(year) + '.json'
print(filename)
with open(filename) as json_file:
    summarydata = json.load(json_file)



for gamecode in summarydata:
    try:
        data = summarydata[gamecode]
        for team in data['Stats']:

            for entry in team['PlayersStats']:
                # print(entry['Player'], entry['Plusminus'])

                # add player to dictionary if not already in there
                if entry['Player'] not in players:
                    players[entry['Player']] = {
                        'Plusminus': entry['Plusminus'], 
                        'Games': 1, 
                        'IsStarter': entry['IsStarter'],
                        'Team': entry['Team'],
                        'Points': entry['Points'],
                        'Valuation': entry['Valuation'],
                        'ValAvg': 1,
                    }
                else:
                    # update the plusminus and games played
                    players[entry['Player']]['Plusminus'] += entry['Plusminus']
                    players[entry['Player']]['Games'] += 1
                    players[entry['Player']]['IsStarter'] += entry['IsStarter']
                    players[entry['Player']]['Points'] += entry['Points']
                    players[entry['Player']]['Valuation'] += entry['Valuation']
                    players[entry['Player']]['ValAvg'] = players[entry['Player']]['Valuation'] / players[entry['Player']]['Games']

    except:
        print('No data for gamecode ' + str(gamecode) + ' in season ' + str(year))

# divide the plusminus by the number of games played
for player in players:
    # round to 1 decimal
    players[player]['Plusminus'] = round(players[player]['Plusminus'] / players[player]['Games'], 1)

# sort the dictionary by plusminus
sorted_players = sorted(players.items(), key=lambda x: x[1]['Games'], reverse=True)


def print_top_players(players, key, top):
    sorted_players = sorted(players.items(), key=lambda x: x[1][key], reverse=True)
    print('Top players by ' + key + ':')

    # print first 10 players
    print('pos player', '\t', 'team', '\t',  '+/-', '\t', 'games', '\t', 'starter',  'points', ' val', '\t', 'avg')
    for nr, player in enumerate(sorted_players[:top]):
        # use tabs to make the output more readable

        # playername shoudl be max 15 characters, fill with spaces if shorter
        playername = player[0].split()[0].replace(',','')[:10]
        playername = playername + ' ' * (10 - len(playername))

        rank = str(nr + 1)
        rank = rank + ' ' * (3 - len(str(rank)))

        print(rank, playername, '\t', player[1]['Team'], '\t', player[1]['Plusminus'], '\t', player[1]['Games'], '\t',  player[1]['IsStarter'], '\t', player[1]['Points'], '\t', player[1]['Valuation'], '\t', int( player[1]['ValAvg'])   )
    print('\n')

print_top_players(players, 'Plusminus', top_players)
print_top_players(players, 'Games', top_players)
print_top_players(players, 'IsStarter', top_players)
print_top_players(players, 'Points', top_players)
print_top_players(players, 'ValAvg', top_players)
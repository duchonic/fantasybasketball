# Import the requests module
import requests
import json 
import sys


# make a dictionary with all the basketball players
players = {}

# create a json document to store the scraped data
summarydata = {}

year = "2024"
euro = "U"
fail_counter = 0
gamecode = 0

if len(sys.argv) == 3:
    euro = sys.argv[1]
    year = sys.argv[2]
elif len(sys.argv) == 2:
    euro = sys.argv[1]
else:
    pass

if euro == 'league':
    euro = 'E'
elif euro == 'cup':
    euro = 'U'


while fail_counter < 10:
    # increment the gamecode
    gamecode += 1
    # Send a GET request to the desired API URL
    url= 'https://live.euroleague.net/api/Boxscore?gamecode=' + str(gamecode) + '&seasoncode=' + euro + year 
    try:
        response = requests.get(url)

        # Parse the response and print it
        data = response.json()

        # apppend the data to the json document
        summarydata[gamecode] = data

        print('gamecode: ', url )

        for team in data['Stats']:
            # print team name with new line in front
            # print('\n', team['Team'])

            for entry in team['PlayersStats']:
                # print(entry['Player'], entry['Plusminus'])

                # add player to dictionary if not already in there
                if entry['Player'] not in players:
                    players[entry['Player']] = {'Plusminus': entry['Plusminus'], 'Games': 1, 'Team': entry['Team']}
                else:
                    # update the plusminus and games played
                    players[entry['Player']]['Plusminus'] += entry['Plusminus']
                    players[entry['Player']]['Games'] += 1
        fail_counter = 0
    except:
        fail_counter += 1
        print('No data : ' + url)


# save the summarydata to a json file on the disk
if euro == 'E':
    euro = 'league'
elif euro == 'U':
    euro = 'cup'
with open('euro' + str(euro) + str(year) + '.json', 'w') as outfile:
    json.dump(summarydata, outfile)